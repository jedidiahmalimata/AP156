#!/usr/bin/env python

"""
Code for Chain Vibrations (W.Kinzel/G.Reents, Physics by Computer)

This code is based on chain.m listed in Appendix E of the book and will
replicate Fig. 2.12 of the book.
"""

__author__ = "Christian Alis"
__credits__ = "W.Kinzel/G.Reents"

import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg import eigvals, inv, eig

# what does ** in **kwargs do?
# ** defines the arguments to be used as keyworded rather than positional (which
# is defined by *)
def main(f=1.0, m1=0.4, m2=1.0, **kwargs):
    """
    Plots the frequencies of four eigenomodes of a linear chain composed of
    one heavy atom and three light atoms which are connected by elastic forces
    as a function of the ave number q
    
    Parameters
    ----------
    f : float
        spring constant of the lattice chain
    m1 : float
        mass of one of the atoms in the lattice chain
    m2 : float
        mass of one of the atoms in the lattice chain
    **kwargs : dict, optional
    """
    print("\n Oscillator Chain \n")
    mass_matrix = lambda q: np.array([[2*f            ,  -f,   0, -f*np.exp(-1j*q)],
                               [-f             , 2*f,  -f,                0],
                               [0              ,  -f, 2*f,               -f],
                               [-f*np.exp(1j*q),   0,  -f,              2*f]])
    # will cyclic rearrangements of m1 and m2 below change the results? Try it
    # Yes
    # what will be the dimensions/shape of massmat?
    # 4 x 4 diagonal matrix
    massmat = np.diag([m1, m1, m1, m2])
    # what happens if inv(massmat) * mass_matrix(q) is used instead of 
    # inv(massmat).dot(mass_matrix(q))?
    # mass_matrix_inv is changed because of the former's non-vector properties
    mass_matrix_inv = lambda q: inv(massmat).dot(mass_matrix(q))
    # what is the python type of kwargs?
    # kwargs is a dictionary
    plot_step = kwargs.get('plot_step', np.pi/50)
    x_axis = np.arange(-np.pi, np.pi, plot_step)
    # what is the difference between eigvals() and eig()?
    # eigvals() computes eigenvalues from an ordinary or generalized eigenvalue
    # problem and find eigenvalues of a general matrix while eig () solves an
    # ordinary or generalized eigenvalue problem of a square matrix and
    # finds eigenvalues w and right or left eigenvectors of a general matrix
    eigenlist = [eig(mass_matrix_inv(x))[0] for x in x_axis]
    plt.plot(x_axis, eigenlist)
    plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi], [r"$\pi$", r"$\frac{\pi}{2}$", "0", r"$\frac{\pi}{2}$",
                r"$\pi$"])
    plt.xlim(-np.pi, np.pi)
    plt.xlabel("q")
    plt.ylabel("omega")
    plt.tick_params('x', labelsize='x-large')
    plt.show()

    # why is the argument of mass_matrix_inv(), 0.0?
    # Because the argument mass_matrix_inv() takes is for q, and for this case,
    # q = 0 is being studied
    # what does the parameter of mass_matrix_inv() mean?
    # The parameter of mass_matrix_inv() is q in the ansatz equation
    eigensys = eig(mass_matrix_inv(0.0))
    return eigensys
    
    # additional: 
    # * rename mass_matrix and mass_matrix_inv to be more descriptive
    # * describe the result if the values of m1 and m2 are interchanged
    # * describe the effect of different values of f
    # optional challenging exercise: animate the eigenmodes as in Fig. 2.13

if __name__ == "__main__":
    print(main())