#!/usr/bin/env python

"""
Code for Chaotic Pendulum (W.Kinzel/G.Reents, Physics by Computer)

This code is based on pendulum.c listed in Appendix E of the book and will
replicate Fig. 4.3 of the book.
"""

__author__ = "Christian Alis"
__credits__ = "W.Kinzel/G.Reents"

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from numpy import sin, cos
from scipy.integrate import odeint

class Simulation(object):
    r = 0.25
    a = 0.7
    lims = 3.0
    dt = 0.1
    pstep = 3*np.pi
    poncaire = False

    def derivs(self, t, y):
        """
        Creates an array for the equation of motion for a chaotic pendulum 
        driven by a periodic torque of strength a and a frequency of 2/3
        
        Parameters
        ----------
        t : list
            contains the different parameters for the equations of motion
        y : list
            contains the instances/measures of the time for the simulation
        """
        return np.array([y[1], - self.r*y[1] - sin(y[0]) + self.a*cos(2./3.*t)])
    
    def rk4_step(self, f, x, y, h):
        """
        Fourth order Runge-Kutta method to solve the differential equation
        f numerically
        
        Parameters
        ----------
        f : func
            differential equation to be solved
        x : list
            x values for the differential equation
        y : list
            y values for the differential equation
        h : float
            step size parameter
        """
        k1 = h*f(x, y)
        k2 = h*f(x + 0.5*h, y + 0.5*k1)
        k3 = h*f(x + 0.5*h, y + 0.5*k2)
        k4 = h*f(x + 0.5*h, y + k3)
        return np.array(y + k1/6. + k2/3. + k3/3. + k4/6.)
    
    def update(self, frame, line, f, x, h, pstep):
        """
        Updates the animation for the plot of the chaotic pendulum in phase space
        
        Parameters
        ----------
        frame : float
            size of the frame
        line : tuple
            contains coordinates for plotting the motion of the pendulum in
            phase space
        h : float
            step size parameter
        pstep : float
            Poncaire step
        """
        if self.poncaire:
            self.y.append(self.rk4_step(f, frame*h, self.y[-1], pstep))
        else:
            self.y.append(self.rk4_step(f, frame*h, self.y[-1], h))
        xs, ys = zip(*self.y)
        line.set_xdata(xs)
        line.set_ydata(ys)
    
    def continue_loop(self):
        """
        Makes the simulation run indefinitely
        
        """
        i = 0
        while 1:
            i += 1
            # what does yield do?
            # what will happen if return is used instead of yield?
            # yield continuously outputs the new value of i for use in the
            # simulation, if return instead of yield were used then the
            # simulation would not work
            yield i
    
    def on_key(self, event):
        """
        Defines events that modify the animation
        """
        key = event.key
        if key == 'i':
            self.a += 0.01
            self.info_text.set_text("$r$ = %0.2f\t$a$ = %0.6f" % (self.r,
                                                                  self.a))
        if key == 'd':
            self.a -= 0.01
            self.info_text.set_text("$r$ = %0.2f\t$a$ = %0.6f" % (self.r,
                                                                  self.a))
        elif key == '+':
            self.lims /= 2
            plt.xlim(-self.lims, self.lims)
            plt.ylim(-self.lims, self.lims)
        elif key == '-':
            self.lims *= 2
            plt.xlim(-self.lims, self.lims)
            plt.ylim(-self.lims, self.lims)        
        elif key == 't':
            self.poncaire = not self.poncaire
            if self.poncaire:
                self.line.set_linestyle('')
                self.line.set_marker('.')
                self.y = [np.array([np.pi/2, 0])]
            else:
                self.line.set_linestyle('-')
                self.line.set_marker('')
                self.y = [np.array([np.pi/2, 0])]
    
    def run(self):
        """
        Generates the display for the animation
        """
        fig = plt.figure()
        # what are the other possible events?
        fig.canvas.mpl_connect('key_press_event', self.on_key)
    
        self.y = [np.array([np.pi/2, 0])]
        self.line, = plt.plot([], [])
        plt.xlim(-self.lims, self.lims)
        plt.ylim(-self.lims, self.lims)
        self.info_text = plt.figtext(0.15, 0.85, "$r$ = %0.2f\t$a$ = %0.6f" % (self.r, self.a))
        anim = FuncAnimation(fig, self.update, self.continue_loop, 
                             fargs=(self.line, self.derivs, 0, self.dt, self.pstep),
                             interval=25, repeat=False)
        plt.show()

if __name__ == "__main__":
    sim = Simulation()
    sim.run()