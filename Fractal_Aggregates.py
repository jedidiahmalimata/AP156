import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt

__author__ = 'James Pang'
__name__ = '__main__'

class Simulation(object):
    rmax = 1.
    lmax = 220.
    rs = rmax + 3.
    rd = rmax + 5.
    rkill = 100.*rmax
    xf = np.zeros([lmax, lmax])
    N = 3000.
    rx = 0.
    ry = 0.

    
    def jump(self):
        """
        Jumps to one of four adjacent sites of generated particle
        """
        choice = np.random.randint(0,4)
        if choice == 0:
            self.rx += 1
        elif choice == 1:
            self.rx += -1
        elif choice == 2:
            self.ry += 1
        elif choice == 3:
            self.ry += -1
        
    
    
    def aggregate(self):
        """
        Adds particle to the aggregate once the particle is adjacent to it
        """
        self.xf[self.rx + self.lmax/2][self.ry + self.lmax/2]=1
        self.rmax = max(self.rmax, np.sqrt(self.rx*self.rx+self.ry*self.ry))
    
    def occupy(self):
        """
        Selects start site on the circle of radius R_{s} or the start circle
        """
        phi = np.random.rand()*2*np.pi
        self.rx = self.rs*np.sin(phi)
        self.ry = self.rs*np.cos(phi)
    
    def circlejump(self):
        """
        Adds a random vector of length R - R_{s} to the position of the particle
        """
        phi = np.random.rand()*2*np.pi
        r = np.sqrt(self.rx**2 + self.ry**2)
        self.rx += (r-self.rs)*np.sin(phi)
        self.ry += (r-self.rs)*np.cos(phi)
    
    def check(self):
        """
        Determines what to do with the particle generated after each jump
        (i.e. annihilate, short jump, long "circle jump")
        """
        r = np.sqrt(self.rx**2 + self.ry**2)
        #Checks if the jump is beyond the kill circle
        if r > self.rkill:
            return 'k'
        #Checks if the jump is within the radius R_{d} of the  d circle
        elif r >= self.rd:
            return 'c'
        #Checks if the jump is adjacent to the aggregate
        elif self.xf[self.rx + 1 + self.lmax/2][self.ry + self.lmax/2] + \
        self.xf[self.rx - 1 + self.lmax/2][self.ry + self.lmax/2] + \
        self.xf[self.rx + self.lmax/2][self.ry + 1 + self.lmax/2] + \
        self.xf[self.rx + self.lmax/2][self.ry - 1 + self.lmax/2] > 0.:
            return 'a'
        #If none of the prior conditions are met, the particle is made to take
        #a long "circle jump"
        else:
            return 'j'
    
    
    
    def update(self):
        self.occupy()
        self.jump()
        
        while 1: #while True
            status = self.check()
            if status == 'k':
                self.occupy()
                self.jump()
            elif status == 'a':
                self.aggregate()
                #what will happen if the next three lines are removed
                #try it, explain why such pattern is produced
                self.rs = self.rmax + 3.
                self.rd = self.rmax + 5.
                self.rkill = 100.*self.rmax
                break #put animation here
    
            elif status == 'j':
                self.jump()
            elif status == 'c':
                self.circlejump()
    
    def run(self):
        #what is the significance of the ff line?
        self.xf[self.lmax/2][self.lmax/2] = 1
        for __ in range(1, self.N):
            self.update()
         
if __name__ == "__main__":
    sim = Simulation()
    sim.N = 3000 #number of points
    #to update number of lattice sim.lmax = ..
    sim.run()
    #what happens when cmap = cm.gray
    plt.matshow(sim.xf, cmap = cm.gray_r)
    plt.show()
    
#try changing the value of N and see the resulting patterns
#Note that the simulation speed is greatly affected by number of particles N
#Improve the code by adding a fractal dimension measurement

#OPTIONAL EXERCISE: Show as animation each time 
#add a fractal dimension measure
#note that
#show an animation each time a particle joins an aggregate