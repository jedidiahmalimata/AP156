import matplotlib.pyplot as plt
import numpy as np

s, steps = 0, 10000
p1 = (np.random.random(), np.random.random())
p2 = (np.random.random(), np.random.random())
p3 = (np.random.random(), np.random.random())
q0 = (np.random.random(), np.random.random())
x_list, y_list = [q0[0]], [q0[1]]


while s < steps:
    #Choosing one of the random point p_{1} to p_{3}
    i = np.random.randint(0,3)
    
    if i == 0:
        p = p1
    elif i == 1:
        p = p2
    else:
        p = p3

    px = p[0]
    py = p[1]   
    qx = q0[0]
    qy = q0[1]
    #Computes for the coordinates of q_{n}
    qx_next = (px + qx) / 2
    qy_next = (py + qy) / 2
    x_list.append(qx_next)
    y_list.append(qy_next)
    s += 1
    q0 = (qx_next, qy_next)        
    
plt.plot(x_list, y_list, 'k.')