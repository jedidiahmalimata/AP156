import numpy as np

N = 20
modspins = np.zeros([N,N],float)

for n in range(N):
    for m in range(N):
        spin = (np.random.randint(0,2))-1
        if spin==0:
            spin +=1
        modspins[n,m] = spin


#calculate the energy
def E(modspins):
    prodbycolumn = np.zeros([N-1,N-1],float)
    prodbyrow = np.zeros([N-1,N-1],float)
    for i in range(N-1):
        prodbyrow[i,0:N-1] = modspins[i,0:N-1]*modspins[i+1,0:N-1]
        prodbycolumn[0:N-1,i] = modspins[0:N-1,i]*modspins[0:N-1,i+1]

    J = 1.0 #sample value of J
    E = 0.0

    for x in range(N-1):
        for y in range(N-1):
            E+=-J*prodbyrow[x,y]
            E+=-J*prodbycolumn[x,y]

    return E