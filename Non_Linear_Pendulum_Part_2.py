# -*- coding: utf-8 -*-
"""
Created on Sat Dec  3 02:29:10 2016

@author: Jethro
"""

from scipy.special import ellipk
from scipy.integrate import quad
from time import time
import numpy as np

start = time()

def T(phi0):
    return 4*ellipk(np.sin(phi0/2)**2)

phis = np.linspace(0, 3.14, 200)
ts = T(phis)

print("Period T as a function of phi0 using scipy.special.ellipk: \n")
print(ts)
end = time()
print("\n Time to implement scipy.special.ellipk:", end-start, "seconds")

start2 = time()

ts3 = np.zeros(200, float)
error = np.zeros(200, float)

for i in range(200):
    ts2 = lambda x: 4*(1/(np.sqrt(1-((np.sin(phis[i]/2)**2)*(np.sin(x)**2)))))
    ts3[i], error[i] = (quad(ts2, 0, np.pi/2))

print("Period T as a function of phi0 using scipy.integrate.quad: \n")
print(ts3)
end2 = time()
print("\n Time to implement scipy.integrate.quad:", end2-start2, "seconds")