# -*- coding: utf-8 -*-
"""
Created on Wed Oct 19 09:47:56 2016

@author: Jethro
"""

import numpy as np
import matplotlib.pylab as plt

p = np.arange(0.1,1, 0.1)
s = 20
cluster_mass = np.zeros((len(p), 10), float)

for v in range(len(p)):
    for u in range(10):
        count, particle = 1, 0
        lattice = np.random.random([s,s]) < p[v]
        label = np.zeros((s,s), dtype = int)
        map1, top_cluster, bottom_cluster, label_list = [], [], [], []
        
        for i in range(s):
            for j in range(s):
                if (i == 0) and (j == 0):
                    if lattice[i,j] == True:
                        label[i,j] = count
                        
                elif (i != 0) and (j == 0):
                    if lattice[i,j] == True:
                        if lattice[i-1,j] == True:
                            label[i,j] = label[i-1,j]
                        else:
                            count += 1
                            label[i,j] = count
                
                elif (i == 0) and (j != 0):
                    if lattice[i,j] == True:
                        if lattice[i,j-1] == True:
                            label[i,j] = label[i,j-1]
                        else:
                            count += 1
                            label[i,j] = count
        
                elif (i != 0) and (j != 0):
                    if lattice[i,j] == True:
                        if (lattice[i,j-1] == True) and (lattice[i-1,j] == True):
                            if label[i,j-1] > label[i-1,j]:
                                label[i,j] = label[i-1,j]
                                map1.append([label[i,j-1], label[i-1,j]])                        
                                label[i,j-1] = label[i-1,j]
                            else:
                                label[i,j] = label[i,j-1]
                                map1.append([label[i-1,j], label[i,j-1]])
                                label[i-1,j] = label[i,j-1]
                        elif (lattice[i,j-1] == True) and (lattice[i-1,j] != True):
                            label[i,j] = label[i,j-1]
                        elif (lattice[i,j-1] != True) and (lattice[i-1,j] == True):
                            label[i,j] = label[i-1,j]
                        else:
                            count += 1
                            label[i,j] = count
        
        for f in range(len(map1)):
            for i in range(s):
                for j in range(s):
                    if label[i,j] == map1[f][0]:
                        label[i,j] = map1[f][1]
            label = label - (label == map1[f][0])*map1[f][0] + (label == map1[f][0])*map1[f][1]
         
        for __ in range(s):
            top_cluster.append(label[0,__])
            bottom_cluster.append(label[s-1,__])
        
        infinite_cluster = list((set(top_cluster) & set(bottom_cluster)))
        
        if 0 in infinite_cluster:
            infinite_cluster.remove(0)
        
        label_list = list(np.unique(label))
        label_list.remove(0)
        
        for __  in range(len(infinite_cluster)):
            for i in range(s):
                for j in range(s):
                    if label[i,j] == infinite_cluster[__]:
                        label[i,j] = 0
        
        for i in range(s):
            for j in range(s):
                if label[i,j] != 0:
                    particle += 1
        
        cluster_mass[v,u] = particle/len(label_list)

average_mass = 0
average_list = np.zeros(len(p), float)

for v in range(len(p)):
    for u in range(10):
        average_mass += cluster_mass[v,u]
    average_mass = average_mass / 10
    average_list[v] = average_mass

plt.plot(p, average_list, 'k--o')
plt.xlabel("Concentration")
plt.ylabel("Average cluster mass of finite clusters")