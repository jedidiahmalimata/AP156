#!/usr/bin/env python

"""
Code for Population Dynamics (W.Kinzel/G.Reents, Physics by Computer)

This code is based on logmap.m listed in Appendix E of the book and will
replicate Figs. 3.1-3.7 of the book.
"""

__author__ = "Christian Alis"
__credits__ = "W.Kinzel/G.Reents"

import numpy as np
import matplotlib.pyplot as plt

f = lambda x, r: 4 * r * x * (1-x)

def iterf(f, x0, r, n):
    """
    Obtains the n-fold iteration of a given function
    
    
    Parameters
    ----------
    f : function
        Non linear function to be iterated
    x0 : float
        Initial value that will serve as a 'seed' for the non linear function
        to be iterated
    r: float
        Parameter that can can be changed within the range [0,1] dependoing on
        the case
    n : int
        Number of iterations for the given non linear function        
    """
    
    # Is it possible to vectorize this function? Explain why or why not.
    #It is not possible to vectorize this function since the function works
    #sequentially.
    out = [x0]
    for _ in range(n):
        out.append(f(out[-1], r))
    return out

def plot_3_1(f=f, x0=0.65, r=0.87, maxiter=40):
    """
    PLots the values of x with respect to n for the n-fold iteration of the
    given non linear function
    
    
    Parameters
    ----------
    f : function
        Non linear function to be iterated
    x0 : float
        Initial value that will serve as a 'seed' for the non linear function
        to be iterated
    r : float
        Parameter that can can be changed within the range [0,1] depending on
        the case
    maxiter : int
        Number of iterations for the given non linear function
    """
    ns = range(maxiter+1)
    xs = iterf(f, x0, r, maxiter)
    plt.plot(ns, xs, '--o')
    plt.xlim(0, maxiter+1)
    plt.xlabel('n')
    plt.ylabel('x(n)')
    plt.show()

def plot_3_2(f=f, r=0.87):
    """
    PLots each iteration of the given function
    
    
    Parameters
    ----------
    f : function
        Non linear function to be iterated
    r : float
        Parameter that can can be changed within the range [0,1] depending on
        the case
    """
    x = np.linspace(0, 1, 500)
    fx1 = f(x, r)
    fx2 = f(fx1, r)
    # complete the following two lines
    fx3 = f(fx2, r)
    fx4 = f(fx3, r)
    plt.plot(x, x, 'k')
    plt.plot(x, fx1, '--', color='gray')
    # What is the python type of the value of color?
    #Colors can be specified in various methods. They can be expressed as color
    #abbreviation, in full names, as hex strings, as RGB/RGBA tuples, and as
    #grayscale intensities as a string.
    # What would happen if (0.6)*3 is used instead of (0.6,)*3?
    #The plot would not show since it would not be able to convert the argument
    #into an RGB sequence.
    # What would happen if (0.3,)*3 is used instead of (0.6,)*3?
    #The plot would become darker.
    # What would happen if (0.9,)*3 is used instead of (0.6,)*3?
    #The plot would become lighter.
    plt.plot(x, fx2, color=(0.6,)*3)
    plt.plot(x, fx4, 'k')
    plt.xlabel('x')
    plt.ylabel('f(x)')
    plt.show()
    
def plot_3_3(x0=0.3, r_min=0, r_max=1):
    """
    Plots 1000 x(n) values for each r within a given range after an initial
    transition periond of 100 steps
    
    Parameters
    ----------
    x0: float
        Initial value that will serve as a 'seed' for the non linear function
        to be iterated
    r_min : float
        Initial value of r for which x(n) values will be generated with; can be
        used to create a range of r
    r_max : float
        Final value of r for which x(n) values will be generated with; can be
        used to create a range of r
    """
    rs = np.linspace(r_min, r_max, 500)
    # What is the purpose of multiplying the output of np.ones() and a number?
    #It changes the value of each element to the value of the number multiplied.
    # What are the dimensions of fxs?
    #len(rs) by 1000
    # What do the rows and columns of fxs represent?
    #Each row is a specific x_n value for various r values.
    #Each column is a set of x values for a particular r value.
    fxs = np.array(iterf(f, np.ones(len(rs))*0.3, rs, 1000))
    # What are the dimensions of fxs?
    #len(rs), which is 500, by 1001
    # What does .T in the second argument do?
    #It ensures that x and y have the same first dimension.
    # Can we use fxs[100:,:] instead of fxs[100:,:].T? Why or why not?
    #No. Since matlplotlib.pylplot can't plot when the x and y values do not
    #have the same first dimension.
    # What does the keyword ms do?
    #It changes the size of the points in the plot.
    plt.plot(rs, fxs[100:,:].T, 'k.', ms=2)
    plt.xlabel('r')
    plt.ylabel('x(n) for 1000 steps after transition period')
    plt.show()
 
def plot_3_4(x0=0.3):
   """
   Plots 1000 x(n) values for each r within a given range after an initial
   transition periond of 100 steps

   Parameters
   ----------
   x0: float
       Initial value that will serve as a 'seed' for the non linear function
       to be iterated
   """
   plot_3_3(r_min=0.88)
   
def plot_3_5(x0=0.3, r = 0.934):
    """
    Plots the frequency distribution of x(n) values in the iteration of the
    parabola
    
    Parameters
    ----------
    x0: float
        Initial value that will serve as a 'seed' for the non linear function
        to be iterated
    r : float
        Parameter that can can be changed within the range [0,1] depending on
        the case
    """    
    # Complete the following line
    fxs = iterf(f, x0, r, 1000)
    plt.hist(fxs, bins=100)
    plt.xlim(xmin=0)
    plt.xlabel('x(n) values')
    plt.ylabel('Frequency of x(n) values')
    plt.show()

def plot_3_6(x0=0.3, r=0.874640, maxiter=1000):
    """
    Plots the sequence of points (x(n), f(x(n))) for a particular function f
    
    Parameters
    ----------
    x0: float
        Initial value that will serve as a 'seed' for the non linear function
        to be iterated
    r : float
        Parameter that can can be changed within the range [0,1] depending on
        the case
    maxiter : int
        Number of iterations for the given non linear function
    
    """
    xs = np.linspace(0, 1)
    fxs = iterf(f, x0, r, 1000)[100:]
    plt.plot(xs, xs)
    plt.plot(xs, f(xs, r))
    # What are the first 10 elements of the first argument of the ff. 
    # plt.plot() call?
    # The first cycle
    # Why was the last element of fxs omitted in the argument of np.repeat()?
    # Not omitting it would make the dimensions of x and y uneven and therefore
    # cannot be plotted
    # Can we use fxs[:-1]*2 instead of np.repeat(fxs[:-1], 2)? Why or why not?
    # No, since they output different values
    plt.plot(np.repeat(fxs[:-1], 2), 
    # What does the following argument of plt.plot() call represent?
    # It represents different instances for indexing fxs to match the first
    # argument of the plt.plot
    # What are the dimensions/shape of the following plt.plot() argument?
    # A 1D array with the same length as the np.repeat() argument
    # What are the dimensions of the argument of np.column_stack()?
    # Two lists with 900 elements each
    # Why was the first element of fxs[1:] omitted?
    # To identify a point on the curve So that it would iterate with two points
    # having the same x or y coordinate and creating straight lines with
    # horizontal and vertical alignment
    # What would happen if .ravel() is removed?
    # The arguments of the plot won't have the same first dimension
    # Can we use np.ravel(zip(fxs[:,1], fxs[1:])) instead of 
    # np.column_stack((fxs[:-1], fxs[1:])).ravel()? Why or why not?
    # No, since it outputs tuples which cannot be taken as an argument
             np.column_stack((fxs[:-1], fxs[1:])).ravel(), 
             '-o')
    plt.xlabel('x(n) values')
    plt.ylabel('Frequency of x(n) values')
    plt.show()


def plot_3_7(x0=0.3, r=0.96420, maxiter=1000):
    """
    Plots the sequence of points (x(n), f(x(n))) for a particular function f
    
    Parameters
    ----------
    x0: float
        Initial value that will serve as a 'seed' for the non linear function
        to be iterated
    r : float
        Parameter that can can be changed within the range [0,1] depending on
        the case
    maxiter : int
        Number of iterations for the given non linear function
    
    """
    
    # complete the following line
    plot_3_6(x0, r, maxiter)
  

# OPTIONAL:
# * Replace np.ones(len(rs))*0.3 with an equivalent expression that uses
#   the Python standard library only (no numpy)
# * write iterf() as a recursive function
# * estimate the Feigenbaum constant
# * add interactivity to your plots

if __name__ == "__main__":
    plot_3_1()
    plot_3_2()
    plot_3_3()
    plot_3_4()
    plot_3_5()
    plot_3_6()
    plot_3_7()